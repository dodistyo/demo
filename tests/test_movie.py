import pytest
import json
from os.path import join, dirname
from jsonschema import validate
import sys
sys.path.append(sys.path[0] + "/..")
from app import Movie, server

@pytest.fixture
def app():
    app = server
    return app

#Helper
def assert_valid_schema(data, schema_file):
    """ Checks whether the given data matches the schema """

    schema = _load_json_schema(schema_file)
    return validate(data, schema)


def _load_json_schema(filename):
    """ Loads the given schema file """

    relative_path = join('schemas', filename)
    absolute_path = join(dirname(__file__), relative_path)

    with open(absolute_path) as schema_file:
        return json.loads(schema_file.read())
#EndOfHelper

def test_movie():
    self = Movie
    assert_valid_schema(Movie.get(self)[0],'movie.json')
        
def func(x):
        return x + 1

def test_answer():
    assert func(4) == 5
