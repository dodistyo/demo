from flask import Flask
import requests
from flask_restx import Api, Resource, fields
from bs4 import BeautifulSoup

server = Flask(__name__)
api = Api(server, version='1.0', title='Pretty Cool API!', description='Awesome API')

app = api.namespace('todos', description='TODO operations')

todo = api.model('Todo', {
    'id': fields.Integer(readOnly=True, description='The task unique identifier'),
    'task': fields.String(required=True, description='The task details')
})
#ini comment dodi
class Todo(object):
    def __init__(self):
        self.todos = [
            {'id': 1,'task': 'Code'},
            {'id': 2,'task': 'Test'},
            {'id': 3,'task': 'Deploy'},
        ]

    def get(self, id):
        for todo in self.todos:
            if todo['id'] == id:
                return todo
        api.abort(404, "Todo {} doesn't exist".format(id))

    def create(self, data):
        todo = data
        self.todos.append(todo)
        return self.todos

    def update(self, id, data):
        todo = self.get(id)
        self.todos.remove(todo)
        self.todos.append(data)
        return self.todos

    def delete(self, id):
        todo = self.get(id)
        self.todos.remove(todo)

OBJ = Todo()

@app.route('/')
class TodoList(Resource):
    '''Shows a list of all todos, and lets you POST to add new tasks'''
    @app.doc('list_todos')
    @app.marshal_list_with(todo)
    def get(self):
        #import ipdb; ipdb.set_trace()
        '''List all tasks'''
        return OBJ.todos

    @app.doc('create_todo')
    @app.expect(todo)
    @app.marshal_with(todo, code=201)
    def post(self):
        '''Create a new task'''
        return OBJ.create(api.payload), 201

#ini comment bd
@app.route('/<int:id>')
@app.response(404, 'Todo not found')
@app.param('id', 'The task identifier')
class Todo(Resource):
    '''Show a single todo item and lets you delete them'''
    @app.doc('get_todo')
    @app.marshal_with(todo)
    def get(self, id):
        '''Fetch a given resource'''
        return OBJ.get(id)

    @app.doc('delete_todo')
    @app.response(204, 'Todo deleted')
    def delete(self, id):
        '''Delete a task given its identifier'''
        OBJ.delete(id)
        return '', 204

    @app.expect(todo)
    @app.marshal_with(todo)
    def put(self, id):
        '''Update a task given its identifier'''
        return OBJ.update(id, api.payload)

movie_api = api.namespace('movie', description='Movie Rank')
movie = api.model('Movie', {
    'rank': fields.Integer(readOnly=True, description='Movie Rank'),
    'movie_title': fields.String(required=True, description='Movie Title')
})
@movie_api.route('/')
class Movie(Resource):
    @app.doc('list_movies')
    @app.marshal_list_with(movie)
    def get(self):
        movies = []
        headers = requests.utils.default_headers()
        url = "https://www.imdb.com/chart/top?ref_=nv_mv_250"
        req = requests.get(url, headers)
        soup = BeautifulSoup(req.content, 'html.parser')
        for crawler in soup.select("td.titleColumn", limit=10):
            movies.append({'rank': int(crawler.find(text=True).lstrip().split('.')[0]), 'movie_title': crawler.select_one('a').get_text()})
        
        return movies
    
        

if __name__ == '__main__':
    server.run(host= '0.0.0.0', debug=True)
